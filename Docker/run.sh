docker run -it --rm \
    -v ${PWD}:/home \
    -p 3000:3000 \
    --network="host" \
    -e MONGO_URL=mongodb://localhost:27017 \
    --name reactjs_practice \
    reactjs_practice:latest
