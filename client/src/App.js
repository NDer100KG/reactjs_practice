import React from 'react';
import { CustomProvider, Loader } from "rsuite"
import axios from 'axios'
import './dark.less'
import Labeler from './components/Labeler';
import MyHeader from './components/MyHeader';
import DBSelector from './components/DBSelector';

class App extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			loaded: null
		}
		console.log(this.state.loaded)
	}

	componentDidMount() {
		console.log(window.location)
		axios.get(`/`)
			.then(({ data }) => {
				this.setState({
					loaded: true
				})
			})
			.catch(() => this.setState({ loaded: false }))

	}

	render() {
		const { loaded } = this.state;
		return (<CustomProvider theme='dark'>
			<MyHeader />
			{/* <DBSelector /> */}

			{loaded === true && <Labeler />}
			{loaded === false && <Loader barkdrop size="lg" content="log/ directory not found" vertical />}
			{loaded === null && <Loader backdrop size="lg" content="loading..." vertical />}
		</CustomProvider>)
	}
}

export default App;
