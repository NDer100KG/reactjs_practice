import React from "react"
import { Grid, Row, Col, ButtonToolbar, Button, Panel } from 'rsuite';
import logo from "../logo.svg"
import axios from 'axios'
import ReactJson from 'react-json-view'


class Labeler extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            data_info: { "id": null, "path": null },
            all_data_idx: null,
            cursor: -1,
            img: logo,
            num_data: 0,
        }
    }

    get_all_sampleIdx() {
        axios.get("/id")
            .then(({ data }) => {
                this.setState({
                    all_data_idx: data,
                    cursor: 0,
                    num_data: data.length,
                })
            })
            .catch(() => this.setState({ error: true }))
    }

    getImage() {
        axios.get("/info_by_id", { params: { "_id": this.state.cursor } })
            .then(({ data }) => {
                console.log(data)
                this.setState({
                    img: require("../" + data.path),
                    data_info: data
                })
            })
            .catch(() => this.setState({ error: true }))
    }

    SetData() {
        axios.post("/set_data", this.state.data_info)
            .then((response) => console.log(response.statusText))
            .catch((error) => console.log(error))
    }

    getNextImage() {
        if (this.state.loaded === null) {
            console.log("No data received yet")
        } if (this.state.cursor === this.state.num_data - 1) {
            console.log("No more data to be loaded")
        }
        else {
            this.state.cursor += 1
            this.getImage()
        }
    }

    getPreviousImage() {
        // axios.post("/image")
        if (this.state.loaded === null) {
            console.log("No data received yet")
        } if (this.state.cursor === 0) {
            console.log("No prev data to be loaded")
        }
        else {
            this.state.cursor -= 1
            this.getImage()
        }

    }

    componentDidMount() {
        this.get_all_sampleIdx()

        document.addEventListener("keypress", this.onKeyPress)
    }

    onKeyPress = (e) => {
        switch (e.key) {
            case "a":
                this.getPreviousImage()
                break;
            case "d":
                this.getNextImage()
                break;
            default:
                break;
        }
    }


    onClick1 = (e) => {
        this.state.data_info["class"] = "red"
        this.SetData()
        this.getNextImage()
    }
    onClick2 = (e) => {
        this.state.data_info["class"] = "orange"
        this.SetData()
        this.getNextImage()
    }
    onClick3 = (e) => {
        this.state.data_info["class"] = "yellow"
        this.SetData()
        this.getNextImage()
    }



    render() {
        return (
            <div>
                <Grid fluid>
                    <Row className="show-grid">
                        <Col xs={16} align="middle">
                            <Row>
                                <img src={this.state.img} width="640" height="480" />
                            </Row>
                            <Row>
                                <ButtonToolbar>
                                    <Button color="red" appearance="primary" onClick={this.onClick1}>Red</Button>
                                    <Button color="orange" appearance="primary" onClick={this.onClick2}>Orange</Button>
                                    <Button color="yellow" appearance="primary" onClick={this.onClick3}>Yellow</Button>
                                </ButtonToolbar>

                            </Row>
                        </Col>
                        <Col xs={8}>
                            <ReactJson theme="monokai" src={this.state.data_info}></ReactJson>
                        </Col>
                    </Row>
                </Grid>

            </div >
        )
    }
}

export default Labeler