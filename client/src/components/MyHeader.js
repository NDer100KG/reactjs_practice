import React from "react"
import { Navbar, Nav, Dropdown } from "rsuite"
// import { Container, NavDropdown, Navbar, Nav } from "react-bootstrap"
import GearIcon from '@rsuite/icons/Gear'
import logo from "../logo.svg"

class MyHeader extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div>
                <Navbar>
                    <Navbar.Brand href="#home">
                        {/* <img alt="" src={logo} width="30px" className="d-inline-block align-top" />{' '} */}
                        <GearIcon spin style={{ fontSize: '2em' }} width="30" height="30" />
                    </Navbar.Brand>
                    <Nav>
                        <Nav.Item>Home</Nav.Item>
                        <Nav.Item>News</Nav.Item>
                        <Nav.Item>Products</Nav.Item>
                        <Dropdown title="About">
                            <Dropdown.Item>Company</Dropdown.Item>
                            <Dropdown.Item>Team</Dropdown.Item>
                            <Dropdown.Item>Contact</Dropdown.Item>
                        </Dropdown>
                    </Nav>
                    <Nav pullRight>
                        <Nav.Item >Settings</Nav.Item>
                    </Nav>
                </Navbar>
            </div>
        )
    }
}
export default MyHeader