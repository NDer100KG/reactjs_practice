import React from "react"
import ReactJson from 'react-json-view'
import { Modal } from "rsuite"

class JsonModal extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            context: {},
        }
    }

    show = (context) => {
        this.setState({ context: context })
    }

    render() {
        return (
            <Modal>
                <ReactJson src={this.state.context}></ReactJson>
            </Modal>
        )
    }
}
export default JsonModal