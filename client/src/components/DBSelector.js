import React from "react"
import DropdownButton from "react-bootstrap/DropdownButton";
import Dropdown from "react-bootstrap/Dropdown";
import axios from 'axios'

class DBSelector extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            onSelect: props.onSelect,
            selected_idx: 0,
            options: [],
        }
    }

    get_samples() {
        axios.get(`/collection`)
            .then(({ data }) => {
                this.setState({
                    options: [<option value='' key="no-selection">Please select...</option>]
                        .concat(data.map(r => (
                            <option key={r._id}>{r.path}</option>
                        )))
                })
            })
            .catch(() => this.setState({ error: true }))
    }



    componentDidMount() {
        console.log("componentDidMount")
        this.get_samples()
    }

    onSelect = (event) => {
        console.log("select")
        this.state.onSelect(event.target.value)
    }


    render() {
        return (
            <div>
                <select className='form-control' onChange={this.select}>{this.state.options}</select>
            </div>

        )
    }
}
export default DBSelector