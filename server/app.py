from flask import Flask, jsonify, request
import pymongo


app = Flask(__name__)

db_client = pymongo.MongoClient("localhost", 27017)
db_database = db_client["test_db"]
db_collections = db_database["test_collection"]


@app.route("/collection", methods=["GET"])
def getCollections():
    all_documents = list(db_collections.find({}))
    return jsonify(all_documents)


@app.route("/id", methods=["GET"])
def getCollecionsIds():
    return jsonify(db_collections.find().distinct("_id"))


@app.route("/info_by_id", methods=["GET"])
def getImage():
    id_get = request.args.get("_id")
    document = db_collections.find_one({"_id": id_get})
    return jsonify(document)


@app.route("/set_data", methods=["POST"])
def setData():
    db_collections.find_one_and_replace({"_id": request.json["_id"]}, request.json)
    return jsonify({})


if __name__ == "__main__":
    app.run(debug=False)
