import pymongo
from glob import glob
from rich.console import Console

console = Console()


db_client = pymongo.MongoClient("localhost", 27017)
db_database = db_client["test_db"]
db_collections = db_database["test_collection"]

all_images = sorted(glob("client/src/media/test_imgs/*.jpg"))

db_data = []
for id, img in enumerate(all_images):
    tmp = {}
    tmp["_id"] = str(id)
    img = img.replace("client/src/", "")
    tmp["path"] = img
    db_data.append(tmp)

console.print(db_data)
db_collections.insert_many(db_data)
console.print("done", style="bold green")

