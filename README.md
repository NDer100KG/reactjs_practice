# ReactJs_Practice
React.js practice with reading and writing in mongo db, using python Flask as backend server

My first try using React.js, dirty codes HAHAHA

## MongoDB

1. [installation](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/)
    ```
    wget -qO - https://www.mongodb.org/static/pgp/server-5.0.asc | sudo apt-key add -
    echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/5.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-5.0.list
    sudo apt-get update
    sudo apt-get install -y mongodb-org
    ```
2. Start
    ```
    sudo systemctl start mongod
    sudo systemctl status mongod
    sudo systemctl enable mongod
    ```
3. Download MongoDB compass from [here](https://www.mongodb.com/try/download/compass)
4. Build sample db from [client/src/media/test_imgs](client/src/media/test_imgs)
  ```
  python server/build_mongodb.py
  ```
5. Step 4. generates a simple database as following:

   <img src="attachments/simple_db.png" width=60%>


## ReactJS

1.  Install `node.js` and `npm`, for Ubuntu 18.04 or upper
    ```
    sudo apt install nodejs npm
    nodejs --version
    ```

2. Upgrade node.js
    ```
    npm cache clean -f
    npm install -g n
    sudo n stable
    ```

3. Install `webpack` and `Babel`
    ```
    sudo npm install -g create-react-app
    ```

4. Install reactjs dependencies
    ```
    cd client
    npm install
    ```

## Start service
start python Flask server and React.js frontend
```
python server/app.py

(another terminal)
cd client
npm start
```
and you can see something like this in your `localhost:3000`

<img src="attachments/simple_webpage.png" width=60%>

## Label pictures
1. Press key `a`/`d` for requesting prev/next picture from database
2. Press buttons to give labels of current picture and set back to dataset
3. After you have labeled the picture, the json viewer will show updated data the next time

| Before labeling                          | After labeling                          |
| ---------------------------------------- | --------------------------------------- |
| <img src="attachments/before_label.png"> | <img src="attachments/after_label.png"> |

## Run server/client in side docker
1. Build docker image
   ```
   ./Docker/build.sh
   ```
   this will generate a docker image named `reactjs_practice:latest`
2. Start docker container
   ```
   ./Docker/run.sh
   ```
3. Start server
   ```
   (container) cd /home
   (container) python server/app.py
   ```
4. Start client, in another terminal
   ```
   docker exec -it reactjs_practice bash
   (container) cd /home/client
   (container) npm install
   (container) npm start
   ```
5. You can see the same page as running in local side in `localhost:3000`

## node 
How to use dark mode in npm v5 
1. Include `.less`, [reference](https://juejin.cn/post/6914186676755496973)
2. Include dark theme, [reference](https://rsuitejs.com/zh/guide/official-themes/)


## References
[update-node-js-version](https://phoenixnap.com/kb/update-node-js-version)
